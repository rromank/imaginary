**Available formats**  
* `image/jpeg`  
* `image/pjpeg`  
* `image/png`  

---

For test purposes: `index.html`  

---

**Upload image**  
`POST` /upload   
  
**Get image**  
`GET` /image/:name  

**Get info about all images**  
`GET` /info

**Delete image**  
`DELETE` /:name

**Delete all images**  
`DELETE` /