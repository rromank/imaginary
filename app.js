var Express = require('express');
var multer = require('multer');
var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
var sizeOf = require('image-size');
var Jimp = require('jimp');

var app = Express();
app.use(bodyParser.json({limit: '5mb'}));

const imageFolder = __dirname + '/images/';
const port = 80;
const quality = 65;
const maxWidth = 800;

const availibleMimeTypes = ['image/jpeg', 'image/pjpeg', 'image/png'];

const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, imageFolder);
    },
    filename: function (req, file, callback) {
        var extension = file.originalname.split('.').pop();
        callback(null, Date.now() + '.' + extension);
    }
});

const fileFilter = function (req, file, callback) {
    var mimetype = file.mimetype;
    if (availibleMimeTypes.includes(mimetype)) {
        callback(null, true);
    }
    return callback(null, false);
};

var upload = multer({storage: storage, fileFilter: fileFilter}).single('image');

// ========

app.listen(port, function () {
    console.log('Listening to port ' + port);
});

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/index.html");
});

app.get('/image/:name', function (req, res) {
    if (!fs.existsSync(imageFolder + req.params.name)) {
        res.status(404).send({error: 'image not found'});
        return;
    }

    res.set({'Content-Type': 'image/png'});
    res.sendFile(imageFolder + req.params.name);
});

app.post('/upload', function (req, res) {
    res.setHeader('Content-Type', 'application/json');

    upload(req, res, function () {
        if (!req.file) {
            res.status(400).send({error: 'size or mimetype is incorrect'});
            return;
        }

        var filename = req.file.filename;
        Jimp.read(imageFolder + filename).then(function (image) {
            const width = sizeOf(imageFolder + filename).width;
            image
                .resize(width > maxWidth ? maxWidth : width, Jimp.AUTO)
                .quality(quality)
                .write(imageFolder + filename, function () {
                    res.send(fileInfo(filename));
                });
            image
                .resize(256, Jimp.AUTO)
                .quality(quality)
                .write(imageFolder + 'thumbnail_' + filename);
        });
    });
});

app.delete('/', function (req, res) {
    fs.readdir(imageFolder, function (err, files) {
        removeFiles(files);

        res.setHeader('Content-Type', 'application/json');
        res.send({deleted: files.length});
    });
});

app.delete('/:name', function (req, res) {
    if (!fs.existsSync(imageFolder + req.params.name)) {
        res.status(404).send({error: 'image not found'});
        return;
    }

    var files = [req.params.name, 'thumbnail_' + req.params.name];
    removeFiles(files);

    res.send({deleted: files.length});
});

app.get('/info', function (req, res) {
    const response = [];
    if (fs.existsSync(imageFolder)) {
        const files = fs.readdirSync(imageFolder, 'utf8');
        for (var i = 0; i < files.length; i++) {
            var filename = files[i];
            response.push(fileInfo(filename));
        }
    }
    res.setHeader('Content-Type', 'application/json');
    res.send(response);
});

var removeFiles = function (files) {
    files.forEach(function (file) {
        fs.unlink(path.join(imageFolder, file), function (err) {
            console.log('Deleted: ' + file);
        });
    });
};

var fileInfo = function (filename) {
    const stats = fs.statSync(imageFolder + filename);
    const dimensions = sizeOf(imageFolder + filename);
    return {
        name: filename,
        sizeInMegabytes: stats.size / 1000000.0,
        width: dimensions.width,
        height: dimensions.height
    }
};
